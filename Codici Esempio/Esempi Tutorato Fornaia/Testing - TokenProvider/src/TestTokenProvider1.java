import java.util.ArrayList;

public class TestTokenProvider1
{
	TokenProvider tp;
	
	public static void main(String[] args)
	{
		TestTokenProvider1 t = new TestTokenProvider1();
		
		t.testLunghezza();
		t.testSet();
		t.testSucc();
		t.testNext();
	}

	private void testNext()
	{
		init();
		ArrayList<String> tokens = tp.getList();
		
		for(int i = 0; i < tokens.size(); i++)
		{
			for(int j = i+1; j < tokens.size(); j++)
			{
				if(tokens.get(i).equals(tokens.get(j)))
				{
					System.out.println("FAILED	testNext");
					return;
				}
			}
		}
		
		System.out.println("OK	testNext");
		
		for(String h : tokens)
			System.out.println(h);
	}

	private void testSucc()
	{
		init();
		String token = tp.getToken();
		
		for(int i = 0; i < token.length()-1;)
		{
			if(token.charAt(i++) == token.charAt(i))
			{
				System.out.println("FAILED	testSucc");
				return;
			}
		}
		
		System.out.println("OK	testSucc");
	}

	private void testSet()
	{
		init();
		String token = tp.getToken();
		
		String charSet = "123ABC";
		
		for(int i = 0; i < token.length(); i++)
		{
			if(!charSet.contains(token.charAt(i) + ""))
			{
				System.out.println("FAILED	testSet");
				return;
			}
		}
		
		System.out.println("OK	testSet");
	}

	private void init()
	{
		tp = new TokenProvider();
	}

	private void testLunghezza()
	{
		init();
		
		String token = tp.getToken();
		
		if(token.length() == 5)
		{
			System.out.println("OK	testLunghezza");
		}
		else
		{
			System.out.println("FAILED	testLunghezza");
		}
	}

}
