import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TokenProvider
{
	private final int SIZE = 5;
	private List<String> charSet = new ArrayList<>();
	private ArrayList<String> tokens = new ArrayList<>();
	
	public TokenProvider()
	{
		reset();
		
		for(int i = 0; i < 100; i++)
			tokens.add(getToken());
	}
	
	private void reset()
	{
		charSet.clear();
		charSet.add("1");
		charSet.add("2");
		charSet.add("3");
		charSet.add("A");
		charSet.add("B");
		charSet.add("C");
	}
	
	public String getToken()
	{
		String token = "";
		Random r = new Random();
		
		for(int i = 0; i < SIZE; i++)
		{
			int aux = Math.abs(r.nextInt());
			aux = aux % charSet.size();
			
			token += charSet.get(aux);
			String tmp = charSet.get(aux);
			
			reset();
			
			charSet.remove(tmp);
		}

		return token;
	}

	public ArrayList<String> getList()
	{
		return tokens;
	}
}
