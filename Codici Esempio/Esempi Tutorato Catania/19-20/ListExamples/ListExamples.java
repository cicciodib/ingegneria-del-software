import java.util.List;
import java.util.function.Consumer;
import java.util.LinkedList;
import java.util.ArrayList;

import java.util.Arrays;

public class ListExamples {
    public static void main(String[] args) {
        removeExample();
        retainAllExample(); 
        arrayExample();
    }

    protected static List initList() {
        // Dichiarazione lista
        List<String> list = new ArrayList<String>();
        fillList(list);

        return list;
    }

    protected static void arrayExample() {
        System.out.println("-- Esempio inizializzazione e stampa di un array classico");

        String[] array = new String[2];

        array[0] = "Ciao";
        array[1] = "Mondo";

        System.out.println("Stampa senza Arrays.toString: " + array);
        System.out.println("Stampa con Arrays.toString: " + Arrays.toString(array));
    }   

    protected static void removeExample() {
        System.out.println("-- Esempio remove");

        List<String> list = initList();

        System.out.println("Lista iniziale: " + list);

        list.remove("Mondo");

        System.out.println("Lista finale: " + list);
    }

    protected static void retainAllExample() {
        System.out.println("-- Esempio filtraggio di una lista con retainAll");
        List<String> list = initList();
        List<String> filter = new LinkedList<String>();

        System.out.println("Lista: " + list);
        System.out.println("Filtro: " + filter);

        fillFilter(filter);
        list.retainAll(filter);

        System.out.println("Risultato: " + list);
    }

    // Funzioni utili

    protected static void fillList(List<String> list) {
        list.add("Ciao");
        list.add("Mondo");
    }

    protected static void fillFilter(List<String> filter) {
        filter.add("Ciao");
    }
}