package com.tutoratose1920.jexplorer.file.filesystem;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.io.IOException;

public class StorageInterface implements FileSystemInterface {
    protected File root;

    @Override
    public void openRoot(String path) throws IOException {
        root = new File(path);

        if(!root.isDirectory()) {
            throw new IOException("Root is not a directory.");
        }
    }

    @Override
    public boolean isFile(String path) {
        File file = fileFromRelativePath(path);
        
        return file.isFile();
    }

    @Override
    public boolean isDirectory(String path) {
        File file = fileFromRelativePath(path);

        return file.isDirectory();
    }

    @Override
    public String getIdentifier(String path) {
        File file = fileFromRelativePath(path);

        return file.getAbsolutePath();
    }

    @Override
    public String[] getChildren(String path) {
        ArrayList<String> paths = new ArrayList<>();

        for (String filePath : fileFromRelativePath(path).list()) {
            paths.add(Paths.get(path, filePath).toString());
        }

        return paths.toArray(String[]::new);
    }

    protected String getFullPath(String relativePath) {
        return Paths.get(root.getPath(), relativePath).toString();
    }

    protected File fileFromRelativePath(String relativePath) {
        return new File(getFullPath(relativePath));
    }

    @Override
    public Reader getReader(String path) throws IOException {
        try {
            return new FileReader(getFullPath(path));
        } catch(FileNotFoundException e) {
            throw new IOException("File not found");
        }
    }
}