package com.tutoratose1920.jexplorer.file;

import java.io.File;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.WeakHashMap;

import com.tutoratose1920.jexplorer.file.filesystem.FileSystemInterface;

public class FileExplorer {
    protected FileSystemInterface fsInterface;
    protected String rootPath;

    protected WeakHashMap<String, String> cache;

    public FileExplorer(FileSystemInterface fsInterface, String rootPath) throws IOException {
        this.rootPath = rootPath;

        this.fsInterface = fsInterface;
        fsInterface.openRoot(rootPath);

        cache = new WeakHashMap<>();
    }

    public void explore(PrintStream stream) throws IOException {
        scan("/", stream);
    }

    protected void scan(String path, PrintStream stream) throws IOException {
        if(fsInterface.isFile(path)) {
            stream.println(path + " is a regular file, reading content...");

            printFileContent(path, stream);

            return;
        }

        if(fsInterface.isDirectory(path)) {
            stream.println(path + " is a directory, exploring recursively...");

            String[] children = fsInterface.getChildren(path);

            for(String child : children) {
                scan(child, stream);
            }
        }
    }

    protected void printFileContent(String path, PrintStream stream) throws IOException{
        String fileKey = fsInterface.getIdentifier(path);
        String content = null;

        BufferedReader reader = new BufferedReader(fsInterface.getReader(path));

        if(cache.containsKey(fileKey)) {
            stream.println("File content is cached");

            content = cache.get(fileKey);
        } else {
            stream.println("File content is not cached, accessing the filesystem...");

            StringBuilder builder = new StringBuilder();

            String line = reader.readLine();

            while(line != null) {
                builder.append(line);

                line = reader.readLine();
            }

            content = builder.toString();

            stream.println("Saving content in cache...");

            cache.put(fileKey, content);
        }

        stream.println(content);
    }
}