package com.tutoratose1920.jexplorer.file;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.StringReader;

import com.tutoratose1920.jexplorer.file.filesystem.FileSystemInterface;
// import com.tutoratose1920.jexplorer.file.stub.TestFSInterface;

import org.junit.jupiter.api.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.mockito.Mockito.*;

class FileExplorerTests {
	FileSystemInterface testFsInterface;

	void setup() {
		try {
			// testFsInterface = new TestFSInterface();
			testFsInterface = mock(FileSystemInterface.class);
		} catch (final Exception e) {
			System.err.println("Error while initializing default test environment");
		}
	}

	void cleanup() {
		testFsInterface = null;
	}

	@Test
	void testConstructorWithValidPath() throws IOException {
		setup();

		final String testPath = "test/";

		doAnswer(invocation -> null)
			.when(testFsInterface).openRoot(testPath);

		try {
			final FileExplorer fileExplorer = new FileExplorer(testFsInterface, testPath);
		} catch (final IOException e) {
			fail("Exception thrown even if the path was valid.");
		}

		cleanup();
	}

	@Test
	void testConstructorWithNonExistentPath() throws IOException {
		setup();

		final String testInvalidPath = "invalid_path/";

		doAnswer(invocation -> {
				throw new IOException("Test invalid path exception");
		}).when(testFsInterface).openRoot(testInvalidPath);

		try {
			final FileExplorer fileExplorer = new FileExplorer(testFsInterface, testInvalidPath);

			fail("Exception not thrown even if the path was not valid.");
		} catch (final IOException e) {
			// Passed
		}

		cleanup();
	}

	@Test
	void testConstructorWithFilePath() throws IOException {
		setup();

		final String testFilePath = "myfile.txt";

		doAnswer(invocation -> {
				throw new IOException("Test invalid path exception");
		}).when(testFsInterface).openRoot(testFilePath);

		try {
			final FileExplorer fileExplorer = new FileExplorer(testFsInterface, testFilePath);

			fail("Exception not thrown even if the path was not valid.");
		} catch (final IOException e) {
			// Passed
		}

		cleanup();
	}

	@Test
	void testScanWithSingleFile() throws IOException {
		// Setup 
		setup();

		final String testRootPath = "test/";
		final String testFilePath = "myfile.txt";

		doAnswer(invocation -> null)
			.when(testFsInterface).openRoot(testRootPath);

		doAnswer(invocation -> null)
			.when(testFsInterface).getReader(testFilePath);

		when(testFsInterface.isFile(testFilePath)).thenReturn(true);

		final String testFileContent = "contenuto file";
		StringReader mockReader = new StringReader(testFileContent);

		when(testFsInterface.getReader(testFilePath)).thenReturn(mockReader);

		FileExplorer fileExplorer;

		try {
			fileExplorer = new FileExplorer(testFsInterface, testRootPath);
		} catch (final IOException e) {
			fail("Exception while initializing the object");

			return;
		}

		OutputStream outputStream = new ByteArrayOutputStream();
		PrintStream printStream = new PrintStream(outputStream);

		final StringBuilder expectedOutputBuilder = new StringBuilder();
		expectedOutputBuilder.append(testFilePath).append(" is a regular file, reading content...\n");
		expectedOutputBuilder.append("File content is not cached, accessing the filesystem...\n");
		expectedOutputBuilder.append("Saving content in cache...\n");
		expectedOutputBuilder.append(testFileContent).append("\n");

		final String expectedOutput = expectedOutputBuilder.toString();
		
		// Verify
		fileExplorer.scan(testFilePath, printStream);

		String actualOutput = outputStream.toString();

		assertTrue(actualOutput.equals(expectedOutput), "Actual output: '" + actualOutput + "' != Expected output: '" + expectedOutput + "'");

		// Cleanup
		cleanup();
	}
}
