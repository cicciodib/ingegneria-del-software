package jexplorer;

import jexplorer.file.FileExplorer;

public class JExplorer {
    public static void main(String[] args) {
        if(args.length != 1) {
            System.err.println("Wrong count of arguments");
            System.exit(1);
        }

        String filename = args[0];
        System.out.println("Exploring file '" + filename +  "'");

        FileExplorer explorer = new FileExplorer(filename);

        // First time
        long startTime = System.currentTimeMillis();
        try {
            explorer.explore(System.out);
        } catch(Exception e) {
            System.err.println("Error while exploring: " + e);
        }
        long endTime = System.currentTimeMillis();

        System.out.println(" --- Exploring time: " + (endTime - startTime) + " ms"); 
        System.out.println(" --- Exploring again...");

        // Second time
        startTime = System.currentTimeMillis();
        try {
            explorer.explore(System.out);
        } catch(Exception e) {
            System.err.println("Error while exploring: " + e);
        }
        endTime = System.currentTimeMillis();

        System.out.println(" --- Exploring time: " + (endTime - startTime) + " ms"); 

        // Third time
        startTime = System.currentTimeMillis();
        try {
            explorer.explore(System.out);
        } catch(Exception e) {
            System.err.println("Error while exploring: " + e);
        }
        endTime = System.currentTimeMillis();

        System.out.println(" --- Exploring time: " + (endTime - startTime) + " ms"); 
    }
}