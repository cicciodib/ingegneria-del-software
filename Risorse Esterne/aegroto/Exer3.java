/* 
  Dato un array di stringhe di lunghezza 8 , controllare se i caratteri nelle
  posizioni pari (partendo da 0) sono ordinati globalmente. In caso affermativo
  restituire la stringa costituita dalla concatenazione di tutti questi 
  caratteri 
*/
   
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Exer3 {
    private static String prev = "";
        
    public static void main(String []args) {
        ArrayList<String> list = new ArrayList<>();    
        list.add("aaaabbbb");
        list.add("cccdddee");
        list.add("fffggghi");
        list.add("llmnnopp");
        
        String result = list.stream()
            .reduce(null, (tmp, str) -> concatenate(tmp,str));
            
        System.out.println(result);
    }
    
    private static String concatenate(String tmp, String str) {
        if(tmp == null || (!tmp.isEmpty() && checkOrder(prev, str))) {
            tmp = tmp + str.charAt(0) + str.charAt(2) + str.charAt(4) + str.charAt(6);
            prev = str;
            return tmp;
        }
        
        return "";
    }
    
    private static boolean checkOrder(String prev, String str) {
        return 
            prev.charAt(0) < str.charAt(0) &&
            prev.charAt(2) < str.charAt(2) &&
            prev.charAt(4) < str.charAt(4) &&
            prev.charAt(6) < str.charAt(6);
    }
}
