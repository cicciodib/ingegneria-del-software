/* 
  Hai in input una lista di interi positivi, scorrila prendendo tre numeri alla 
  volta a distanza 1 (Quindi per [1,2,3,4,5,6,7] prendi [1,3,5], [2,4,6] e [3,5,7])
  e dire quante di queste terne hanno un elemento pari alla somma degli altri due
*/

import java.util.ArrayList;
import java.util.List;

public class Exer4 {
    private static int[] terna0 = { -1, -1, -1 },
                         terna1 = { -1, -1, -1 };
    private static int switchTerna = 0;
        
    public static void main(String []args){
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        
        long count = list.stream()
                        .map(x -> creaTerna(x))
                        .filter(t -> checkTerna(t))
                        .count();
                        
        System.out.println(count);
    }
    
    private static int[] creaTerna(int x)  {
        switch(switchTerna) {
            case 0:
                terna0[0] = terna0[1];
                terna0[1] = terna0[2];
                terna0[2] = x;
                switchTerna = 1 - switchTerna;
                return terna0.clone();
            case 1:
                terna1[0] = terna1[1];
                terna1[1] = terna1[2];
                terna1[2] = x;
                switchTerna = 1 - switchTerna;
                return terna1.clone();
        }
        
        return null;
    }
    
    private static boolean checkTerna(int[] t) {
        if(t[0] > 0 && t[1] > 0 && t[2] > 0) {
            return 
                t[0] == t[1] + t[2] ||
                t[1] == t[0] + t[2] ||
                t[2] == t[0] + t[1];
        }
        return false;
    }
}
