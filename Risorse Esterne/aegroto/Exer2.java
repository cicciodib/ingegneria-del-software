/*
  Data una lista di stringhe, utilizzando gli stream, ottenere una lista derivata tale che l'i-esima stringa 
  è una stringa di lunghezza pari alla media fra la lunghezza della stringa considerata e la sua precedente 
  (se esiste), ed i caratteri all'interno sono dati concatenando in maniera alternata i caratteri della stringa
   e della precedente fino a riempirla per la lunghezza calcolata, solo se la differenza fra le lunghezze delle
  stringhe è minore di 5. Gestire i casi limite riportando le stringhe affette senza modifiche.
*/

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.lang.Math;

public class Exer2 {
    private static String prev;
    
    public static void main(String []args){
        ArrayList<String> list = new ArrayList<>();
        list.add("You");
        list.add("Just");
        list.add("Get");
        list.add("Tramontato");
        list.add("Lolnoob");
        
        prev = list.get(0);
        
        List<String> newList = 
        list.stream()
            .filter(str -> checkLengths(str))
            .map(str -> combineStrings(str))
            .collect(Collectors.toList());
            
        newList.forEach(str -> System.out.println(str));
    }
     
    private static boolean checkLengths(String str) {
        boolean flag = Math.abs(str.length() - prev.length()) < 5;
        if(!flag)
            prev = str;
        return flag;
    }
    
    private static String combineStrings(String str) {
        if(str.equals(prev)) 
            return str;
            
        int length = (str.length() + prev.length()) / 2,
            currentIndex = 0;
            
        String result = "";
        
        while(currentIndex < length) {
            if(currentIndex % 2 == */)
                result += str.charAt(currentIndex);
            else 
                result += prev.charAt(currentIndex);
                
            ++currentIndex;
        }
        
        prev = str;
        return result;
    }
}
